var { GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLID, GraphQLInt, GraphQLList } = require('graphql');
const mongoose = require('../../mongodb');
const categoryModel = require('../../models/category');

const sub_categoryType = new GraphQLObjectType({
    name: 'sub_categoryType',
    fields: {
        _id: {
            type: GraphQLString
        },
        title: {
            type: GraphQLString
        },
        level: {
            type: GraphQLString
        },
        parent: {
            type: GraphQLString
        }
    }
})

const categoryType = new GraphQLObjectType({
    name: 'category',
    fields: {
        _id: {
            type: GraphQLString
        },
        title: {
            type: GraphQLString
        },
        level: {
            type: GraphQLString
        },
        parent: {
            type: GraphQLString
        },
        sub_categories: {
            type: new GraphQLList(sub_categoryType),
            async resolve (root, params, options) {
                console.log('subcategories',root, params)
                const collectionName =  'category_'+ root._id;
                const category = mongoose.model(collectionName,categoryModel, collectionName);
                return category.find({})
                        .then(result => {
                            return result
                        })
                        .catch(err=> {
                             throw new Error(err)
                        })

            }
        }
    }
})





module.exports = categoryType;