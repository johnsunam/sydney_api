var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLInt, GraphQLList, Gra } = require('graphql');

const taskInputType = new  GraphQLInputObjectType({
    name: 'TaskInput',
    fields: {
         category: {
            type: GraphQLString
        },
        title: {
            type: GraphQLString
        },
        description: {
            type: GraphQLString
        },
        due_type: {
            type: GraphQLString
        },
        due_date: {
            type: GraphQLString
        },
        budget_type: {
            type: GraphQLString
        },
        budget: {
            type: GraphQLInt
        },
        taskerNum: {
            type: GraphQLInt
        },
        userId: {
            type: new GraphQLNonNull(GraphQLString)
        }

    }
})



module.exports = taskInputType

