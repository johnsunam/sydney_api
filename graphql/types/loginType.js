var { GraphQLObjectType, GraphQLNonNull, GraphQLString } = require('graphql');
const userType = require('./user')

const loginType = new  GraphQLObjectType({
    name: 'Login',
    fields: {
        user: {
            type: userType
        },
        token: {
            type: GraphQLString
        }
    }
})

module.exports = loginType;

