const { GraphQLNonNull, GraphQLBoolean, GraphQLObjectType } = require('graphql');

const alertInputType = require('../../types/addAlertInputType');
const alertType = require('../../types/alert');
const AlertModel = require('../../../models/alert');
const mongoose = require('../../../mongodb');

const addAlert = {
    type: alertType,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(alertInputType)
        }
    },
     resolve ( root, params, options ) {
        let data = params.data
       const alertModel = new AlertModel(params.data)
       return  alertModel.save()
               .then(result => result)
               .catch(err => err)
       
    }
}

module.exports = addAlert;