const { GraphQLNonNull, GraphQLBoolean, GraphQLObjectType } = require('graphql');
const _ = require('underscore')

const taskInputTypes = require('../../types/taskInputTypes');
const taskType = require('../../types/task');
const TaskModel = require('../../../models/task');
const mongoose = require('../../../mongodb');

const addTask = {
    type: taskType,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(taskInputTypes)
        }
    },
     resolve ( root, params, options ) {
        let data = params.data
        // let data = _.omit(params.data, 'userId')
        // let collectionName = `task_${userId}`
        data.due_date = new Date(data.due_date)  
        data.createdOn = new Date(Date.now())
        // const Task = mongoose.model(collectionName,TaskModel, collectionName)
        const taskModel = new TaskModel(data)
        return taskModel.save()
               .then(result => result)
               .catch(err => err)
       
    }
}

module.exports = addTask