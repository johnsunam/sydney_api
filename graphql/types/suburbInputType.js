var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLInt, GraphQLList } = require('graphql');

const suburbTypeInput = new GraphQLInputObjectType({
    name: 'suburbTypeInput',
    fields: {
        countryName: GraphQLString,
        countryId: GraphQLInt,
        stateName: GraphQLString,
        stateId: GraphQLInt,
        cityName: GraphQLString,
        cityId: GraphQLInt
    }
})

module.exports = suburbTypeInput