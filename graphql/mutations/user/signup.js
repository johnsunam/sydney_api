const { GraphQLNonNull, GraphQLBoolean, GraphQLObjectType, GraphQLString } = require('graphql');
const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')
const signupInputType = require('../../types/signupInputType')
const UserModel = require('../../../models/user')
const signupType = require('../../types/signupType')

const signupUser = {
    type: signupType,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(signupInputType)
        }
    },
    async resolve ( root, params, options ) {
        console.log('signupppppp========', params)
        params.data.password = await bcrypt.hash(params.data.password, 10)
        const userExist = await UserModel.findOne({email: params.data.email});
        if (userExist) {
            throw new Error('User already exsits with this email address.');
        }

        const userModel = new UserModel(params.data);
        const newUser = await userModel.save();
        if(!newUser) {
            throw new Error('Error adding new user');
            
        }
        const token = jsonwebtoken.sign(
            {
                id: newUser._id,
                email: newUser.email
            },
            'secret',
            { expiresIn: '1d'}
        )
        console.log('token', token)
        return {user: newUser, token: token}
    }
}

module.exports = signupUser;
