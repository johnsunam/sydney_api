const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString } = require('graphql');

const taskType = require('../../types/task');
const getProjection = require('../../get-projection');
const TaskModel = require('../../../models/task');
const helper = require('../../../helper');

const getTasks = {
    type: new GraphQLList(taskType),
    args: {
        userId: {
            type: GraphQLString
        }
     },
    async resolve (root, params, options) {
        console.log(params)
        // const projection = getProjection(options.fieldASTs[0]);
        return TaskModel.find(params).select().exec();
    }
};

module.exports = getTasks;