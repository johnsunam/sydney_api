const getCategories = require('./multiple')
const getCategory = require('./single')
module.exports = {
    getCategories,
    getCategory
}