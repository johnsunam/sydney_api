const { GraphQLList, GraphQLID, GraphQLNonNull } = require('graphql');

const userType = require('../../types/user');
const getProjection = require('../../get-projection');
const UserModel = require('../../../models/user');
const helper = require('../../../helper');

const getUsers = {
    type: new GraphQLList(userType),
    args: { },
    async resolve (root, params, options) {
        // const projection = getProjection(options.fieldASTs[0]);
        return UserModel.find({}).select().exec();
    }
};

module.exports = getUsers;