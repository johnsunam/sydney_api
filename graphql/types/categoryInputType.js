var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLList, GraphQLInt } = require('graphql');

const categoryInputType = new  GraphQLInputObjectType({
    name: 'categoryInput',
    fields: {
        title: {
            type: new GraphQLNonNull(GraphQLString)
        },
        parent: {
            type: GraphQLString
        },
        level: {
            type: GraphQLInt
        }
    }
})

module.exports = categoryInputType;

