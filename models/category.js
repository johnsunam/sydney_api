const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var categorySchema = new Schema({
  title: { type: String },
  level: { type: String },
  parent: { type: String }
}, {
  strict: false
});

module.exports = categorySchema;