const addUser = require('./addUser')
const loginUser = require('./login')
const signupUser = require('./signup')
const updateUserDetail = require('./updateUserDetail')
const deleteUser = require('./deleteUser')


module.exports = {
    addUser,
    loginUser,
    signupUser,
    updateUserDetail,
    deleteUser
}