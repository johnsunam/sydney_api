var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLInt, GraphQLList } = require('graphql');

const taskType = new  GraphQLObjectType({
    name: 'Task',
    fields: {
        _id: {
            type: GraphQLString
        },
         category: {
            type: GraphQLString
        },
        title: {
            type: GraphQLString
        },
        description: {
            type: GraphQLString
        },
        due_type: {
            type: GraphQLString
        },
        due_date: {
            type: GraphQLString
        },
        budget_type: {
            type: GraphQLString
        },
        budget: {
            type: GraphQLInt
        },
        taskerNum: {
            type: GraphQLInt
        }

    }
})



module.exports = taskType

