var { GraphQLObjectType, GraphQLNonNull, GraphQLString } = require('graphql');
const userType = require('./user')

const signupType = new  GraphQLObjectType({
    name: 'Signup',
    fields: {
        user: {
            type: userType
        },
        token: {
            type: GraphQLString
        }
    }
})

module.exports = signupType;

