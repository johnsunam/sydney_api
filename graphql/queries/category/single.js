const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString, Gra } = require('graphql');

const categoryType = require('../../types/category');
const getProjection = require('../../get-projection');
const categorySchema = require('../../../models/category');
const mongoose = require('../../../mongodb');
const categoryModel = require('../../../models/category');

const getCategory = {
    type: categoryType,
    args: { 
        id: { type: GraphQLString },
        parent: { type: GraphQLString }
    },
    async resolve (root, params, options) {
        const { id, parent } ={...params} 
        const collectionName = parent ?   'category_'+ parent : 'category' ;
        console.log('/////////////',id,parent, collectionName)
        const category = mongoose.model(collectionName,categoryModel, collectionName);
        return category.findOne({_id: mongoose.Types.ObjectId(id)})
                .then(result => {
                    return result
                    console.log('resutlllllttttttt',result)
                })
                .catch(err => {
                    throw new Error(err);
                })
    }
};

module.exports = getCategory;