const { GraphQLNonNull, GraphQLBoolean, GraphQLObjectType, GraphQLString } = require('graphql');
const bcrypt = require('bcrypt')
const Boom = require('boom')
const jsonwebtoken = require('jsonwebtoken')
const loginInputType = require('../../types/loginInputType');
const userType = require('../../types/user');
const UserModel = require('../../../models/user');
const loginType = require('../../types/loginType');

const loginUser = {
    type: loginType,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(loginInputType)
        }
    },
    async resolve ( root, params, options ) {
        const user = await UserModel.findOne({email: params.data.email});
        console.log('login in user', user);
        if (!user) {
            throw new Error('No user with this email.');
            // return Boom.unauthorized('User not found!!');
        }

        const valid = await bcrypt.compare(params.data.password, user.password)
        if (!valid) {
            throw new Error('Incorrect password.');

        }
        const token = jsonwebtoken.sign(
            {
                id: user._id,
                email: user.email
            },
            'secret',
            { expiresIn: '1d'}
        )
        console.log('token', token)
        return {user: user, token: token}
    }
}

module.exports = loginUser;
