const addAlert = require('./addAlert')
const addTask = require('./addTask')
const deleteTask = require('./deleteTask')

module.exports = {
    addAlert,
    addTask,
    deleteTask
}