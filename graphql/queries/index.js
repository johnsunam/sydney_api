const user = require('./user');
const category = require('./category');
const task = require('./task')

module.exports = {
    ...user,
    ...category,
    ...task
}