const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString, GraphQLObjectType } = require('graphql');
const TaskModel = require('../../../models/task');

const deleteTask = {
    type: GraphQLString,
    args: {
        id: {
            type: new GraphQLNonNull(GraphQLString)
        },
        userId: {
            type: GraphQLString
        }
    },
    resolve ( root, params, options ) {
        console.log('usersss////////', params)

        return TaskModel
                .findByIdAndRemove(params.id)
                .then(result => {
                    console.log('deleteddddd/////////???????', result)
                    return 'Delete successful!!!'
                })
                .catch(err => {
                    throw new Error(err)
                })
    }
}

module.exports = deleteTask