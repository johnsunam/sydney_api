const { GraphQLNonNull, GraphQLBoolean, GraphQLObjectType } = require('graphql');
const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')
const userInputType = require('../../types/userInputTypes');
const userType = require('../../types/user');
const UserModel = require('../../../models/user');


const addUser = {
    type: userType,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(userInputType)
        }
    },
    async resolve ( root, params, options ) {
        params.data.password = await bcrypt.hash('sydney123', 10)
        const userExist = await UserModel.findOne({email: params.data.email});
        if (userExist) {
            throw new Error('User already exsits with this email address.');
        }

        const userModel = new UserModel(params.data);
        const newUser = await userModel.save();
        if(!newUser) {
            throw new Error('Error adding new user');
            
        }
        console.log('paramssss===', newUser)
        return newUser;
    }
}

module.exports = addUser;
