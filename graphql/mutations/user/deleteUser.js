const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString, GraphQLObjectType } = require('graphql');
const { Types } = require('mongoose')

const userType = require('../../types/user');
const UserModel = require('../../../models/user');

const deleteUser = {
    type: GraphQLString,
    args: {
        id: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve ( root, params, options ) {
        console.log('usersss////////', params)
        // const projection = getProjection(options.fieldASTs[0]);

        return UserModel
                .findByIdAndRemove(params.id)
                .then(result => {
                    console.log('deleteddddd/////////???????', result)
                    return 'Delete successful!!!'
                })
                .catch(err => {
                    throw new Error(err)
                })
    }
}

module.exports = deleteUser