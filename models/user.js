var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String
    },
    firstName: {
        type: String
    },
    lastName:{
        type: String
    },
    suburb:{
        type: Object
    },
    done:{
        type: String
    },
    earn:{
        type: String
    },
    avatar:{
        type: String
    },
    mobile:{
        type: String
    },
    description:{
        type: String
    },
    accHolderName:{
        type: String
    },
    bsb:{
        type: String
    },
    accNum:{
        type: String
    }
});

module.exports = mongoose.model('User', UserSchema);