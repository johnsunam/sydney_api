const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString } = require('graphql');

const categoryType = require('../../types/category');
const getProjection = require('../../get-projection');
const categorySchema = require('../../../models/category');
const mongoose = require('../../../mongodb');
const categoryModel = require('../../../models/category');

const getCategories = {
    type: new GraphQLList(categoryType),
    args: { },
    async resolve (root, params, options) {
        // const { id, parent } ={...params} 
        // const collectionName = id === '' ? 'category' : 'category_'+id ;
        // console.log('///////////','id', id,'parent', parent)    
        const collectionName = 'category';
        const category = mongoose.model(collectionName,categoryModel, collectionName);
        return category.find({}).select().exec();
    }
};

module.exports = getCategories;