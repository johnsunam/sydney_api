const express = require('express');
const express_graphql = require('express-graphql');
const { buildSchema } = require('graphql');
// const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const mongodb = require('./mongodb');
const schema = require('./graphql');
const cors = require('cors');
const ObjectId = require('mongoose').Types.ObjectId;
const env = process.env.NODE_ENV || 'development'

ObjectId.prototype.valueOf = function () {
    return this.toString();
};

require('dotenv').config({
    path: './.env.' + env
});

// mongoose.connect('mongodb://sydney:sydney123@mongo:27017/sydney');

// Create an express server and a GraphQL endpoint
var app = express();
app.use(bodyParser.json({limit: '50mb'}));
app.use(cors())
app.use('/graphql', express_graphql(req => ({
    schema,
    pretty: true,
    graphiql: true,
    formatError(error) {
    console.log(error, error.originalError);
    return {
        message: error.message,
        locations: error.locations,
        path: error.path
    }
    }
})));
// process.env.PORT
// process.env.HOST
// 3000
// 0.0.0.0
const server = app.listen('3000', '0.0.0.0',  () => console.log(`Express GraphQL Server Now Running On localhost:${server.address().port}/graphql`));

