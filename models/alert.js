const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var alertSchema = new Schema({
  userId: { type: String },
  taskKind: { type: String },
  range: { type: Number },
  suburb: { type: String }
}, {
  strict: false
});

module.exports = mongoose.model('Alert', alertSchema);;