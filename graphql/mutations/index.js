const user = require('./user')
const category = require('./category')
const  alert = require('./task')
const task = require('./task')

module.exports = {
    ...user,
    ...category,
    ...alert,
    ...task
}