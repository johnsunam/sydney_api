const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var TaskSchema = new Schema({
  category: { type: String },
  title: { type: String },
  description: { type: String },
  due_type: { type: String },
  due_date: { type: Date },
  budget_type: { type: String },
  budget: { type: Number },
  taskerNum: { type: Number },
  userId: {type: String}
}, {
  strict: false
});

module.exports = mongoose.model('Task', TaskSchema);