var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLInt, GraphQLList } = require('graphql');

const addAlertInput = new  GraphQLInputObjectType({
    name: 'AddAlertInput',
    fields: {
        userId: {
            type: GraphQLString
        },
         taskKind: {
            type: GraphQLString
        },
        range:{
            type: GraphQLInt
        },
        suburb:{
            type: GraphQLString
        }

    }
})



module.exports = addAlertInput;

