const mongoose = require("mongoose");
// mongoose.connect('mongodb://sydney:sydney123@mongo:27017/sydney');
// mongodb://localhost/airtasker
mongoose.connect('mongodb://sydney:sydney123@mongo:27017/sydney', {}, function(err) {
    if(err) {
        throw err;
    }
    else { 
        console.log('Successfully connected to MongoDB Database!');
    }
});

module.exports = mongoose;