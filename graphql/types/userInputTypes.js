var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLList, GraphQLInt } = require('graphql');

const userInputType = new  GraphQLInputObjectType({
    name: 'UserInput',
    fields: {
        email: {
            type: new GraphQLNonNull(GraphQLString)
        },
        role: {
            type: GraphQLString
        },
        firstName: {
            type: GraphQLString
        },
        lastName:{
            type: GraphQLString
        }
    }
})

module.exports = userInputType;

