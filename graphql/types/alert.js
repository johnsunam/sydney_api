var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLInt, GraphQLList } = require('graphql');

const alertType = new  GraphQLObjectType({
    name: 'Alert',
    fields: {
        _id: {
            type: GraphQLString
        },
         taskKind: {
            type: GraphQLString
        },
        range:{
            type: GraphQLInt
        },
        suburb:{
            type: GraphQLString
        }

    }
})



module.exports = alertType;

