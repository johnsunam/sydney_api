const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString } = require('graphql');
const { Types } = require('mongoose')

const taskType = require('../../types/task');
const getProjection = require('../../get-projection');
const TaskModel = require('../../../models/task');
const helper = require('../../../helper');

const getTask = {
    type: taskType,
    args: {
        id: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve ( root, params, options ) {
        return TaskModel
        .findById(params.id)
        .then(result => {
            return result
        })
        .catch(err => {
            throw new Error(err)
        })
        
    }
};

module.exports = getTask;