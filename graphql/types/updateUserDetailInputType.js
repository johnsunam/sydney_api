var { GraphQLInputObjectType, GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLBoolean, GraphQLInt, GraphQLList } = require('graphql');

const updateUserDetailInputType = new  GraphQLInputObjectType({
    name: 'UpdateUserDetailInput',
    fields: {
         firstName: {
            type: GraphQLString
        },
        lastName:{
            type: GraphQLString
        },
        done:{
            type: GraphQLBoolean
        },
        earn:{
            type: GraphQLBoolean
        },
        suburb:{
            type: GraphQLString
        },
        _id:{
            type: GraphQLString
        },
        avatar: {
            type: GraphQLString
        },
        mobile:  {
            type: GraphQLString
        },
        description: {
            type: GraphQLString
        },
        accHolderName:{
            type: GraphQLString
        },
        bsb:{
            type: GraphQLString
        },
        accNum:{
            type: GraphQLString
        }

    }
})



module.exports = updateUserDetailInputType;

