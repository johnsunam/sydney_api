const getUser = require('./single');
const getUsers = require('./multiple');

module.exports = {
    getUser,
    getUsers
}