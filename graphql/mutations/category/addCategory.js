const { GraphQLNonNull, GraphQLBoolean, GraphQLObjectType } = require('graphql');

const categoryInput = require('../../types/categoryInputType');
const categoryType = require('../../types/category');
const CategoryModel = require('../../../models/category');
const mongoose = require('../../../mongodb');

const addCategory = {
    type: categoryType,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(categoryInput)
        }
    },
     resolve ( root, params, options ) {
        const collectionName = params.data.parent ? 'category_'+params.data.parent :'category';
        const Category = mongoose.model(collectionName,CategoryModel, collectionName);
        const newCategory = new Category(params.data);
        return Category.findOne({title: params.data.title})
        .then(async result => {
            console.log('=========', result)
            if (result) {
                throw new Error('Category already exists.')
            } 
              const res = await newCategory.save();
              return res;
        })

       
    }
}

module.exports = addCategory;