const getTasks = require('./multiple')
const getTask = require('./single')

module.exports = {
    getTasks,
    getTask
}