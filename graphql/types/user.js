var { GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLID, GraphQLInt, GraphQLList, GraphQLBoolean } = require('graphql');


// const suburbType = new GraphQLObjectType({
//     name: 'suburbType',
//     fields: {
//         countryName: GraphQLString,
//         countryId: GraphQLInt,
//         stateName: GraphQLString,
//         stateId: GraphQLInt,
//         cityName: GraphQLString,
//         cityId: GraphQLInt
//     }
// })

const userType = new GraphQLObjectType({
    name: 'User',
    fields: {
        _id: {
            type: GraphQLString
        },
        password:{
            type: GraphQLString
        },
        firstName: {
            type: GraphQLString
        },
        lastName:{
            type: GraphQLString
        },
        email: {
            type: GraphQLString
        },
        done:{
            type: GraphQLBoolean
        },
        earn:{
            type: GraphQLBoolean
        },
        suburb:{
            type: GraphQLString
        },
        message: {
            type: GraphQLString
        },
        status: {
            type: GraphQLBoolean
        },
        avatar: {
            type: GraphQLString
        },
        accHolderName:{
            type: GraphQLString
        },
        bsb:{
            type: GraphQLString
        },
        accNum:{
            type: GraphQLString
        }
    }
})





module.exports = userType;