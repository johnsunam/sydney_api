const { GraphQLNonNull, GraphQLBoolean, GraphQLObjectType, GraphQLString } = require('graphql')
const UserModel = require('../../../models/user')
const updateUserDetailInputType = require('../../types/updateUserDetailInputType')
const userType = require('../../types/user')
const _ = require('underscore')

const updateUserDetail = {
    type: userType,
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(updateUserDetailInputType)
        }
    },
    async resolve ( root, params, options ) {
        let _id = params.data._id
        let data = _.omit(params.data,'_id')
        if(data.suburb)
        data.suburb = JSON.parse(data.suburb)
        
        return UserModel.findByIdAndUpdate(_id,{...data})
        .then(result => {
            console.log('updated result',result._id)
            let res = result
            res.suburb = JSON.stringify(result.suburb)
            return res
        })
        .catch(err => {
            return err
        })
    }
}

module.exports = updateUserDetail;
