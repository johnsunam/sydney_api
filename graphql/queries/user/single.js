const { GraphQLList, GraphQLID, GraphQLNonNull, GraphQLString } = require('graphql');
const { Types } = require('mongoose')

const userType = require('../../types/user');
const getProjection = require('../../get-projection');
const UserModel = require('../../../models/user');
const helper = require('../../../helper');

const getUser = {
    type: userType,
    args: {
        id: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve ( root, params, options ) {
        console.log('usersss////////', params)
        // const projection = getProjection(options.fieldASTs[0]);

        return UserModel
                .findById(params.id)
                .then(result => {
                    return result
                })
                .catch(err => {
                    throw new Error(err)
                })
    }
};

module.exports = getUser;